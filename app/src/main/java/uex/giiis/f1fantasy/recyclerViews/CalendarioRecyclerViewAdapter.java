package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.Race;

import static uex.giiis.f1fantasy.ColeccionPaises.obtenerPais;


public class CalendarioRecyclerViewAdapter extends RecyclerView.Adapter<CalendarioRecyclerViewAdapter.ViewHolder> {

    private List<Race> mValues;
    private Context mContext;

    public LigasRecyclerViewAdapter.OnListInteractionListener mListener;

    public CalendarioRecyclerViewAdapter(List<Race> items, LigasRecyclerViewAdapter.OnListInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calendario_fila_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Race item = mValues.get(position);

        holder.mItem = item;
        holder.mNombreView.setText(item.getRaceName());
        holder.mFechaView.setText(item.getDate());

        String pais = item.getCircuit().getLocation().getCountry();
        holder.mPaisView.setImageResource(obtenerPais(pais));

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListCalendario(item.getRound());
            }
        });

    }


    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }


    public List<Race> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombreView;
        public final TextView mFechaView;
        public final ImageView mPaisView;
        public Race mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombreView = (TextView)  view.findViewById(R.id.nombre_calendario);
            mFechaView = (TextView)  view.findViewById(R.id.fecha_calendario);
            mPaisView = (ImageView)  view.findViewById(R.id.pais_calendario);

        }

    }

    public void swap(List<Race> items){
        mValues = items;
        notifyDataSetChanged();
    }

}

