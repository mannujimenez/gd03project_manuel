package uex.giiis.f1fantasy.recyclerViews;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.ui.SplashActivity;


public class GPRecyclerViewAdapter extends RecyclerView.Adapter<GPRecyclerViewAdapter.ViewHolder> {

    private List<Result> mValues;
    private List<Driver> mDrivers;
    private List<Constructor> mConstructors;
    private Context mContext;

    public GPRecyclerViewAdapter(List<Result> items, List<Driver> drivers, List<Constructor> constructors, Context context) {
        mValues = items;
        mDrivers = drivers;
        mConstructors = constructors;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.clasificaciones_fila_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Result item = mValues.get(position);

        holder.mItem = item;
        if (item != null) {
            holder.mPosicionView.setText(item.getPosition());
            for (Driver d : mDrivers) {
                if (item.getIdDriver().equals(d.getDriverId())) {
                    holder.mNombreView.setText(d.getGivenName() + " " + d.getFamilyName());
                }
            }
            for (Constructor c : mConstructors) {
                if (item.getIdConstructor().equals(c.getConstructorId())) {
                    holder.mEscuderiaView.setText(c.getName());
                }
            }
            holder.mPuntosView.setText(item.getPoints());
            holder.mVictoriasView.setText("");
        }
    }


    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }


    public List<Result> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPosicionView;
        public final TextView mNombreView;
        public final TextView mEscuderiaView;
        public final TextView mPuntosView;
        public final TextView mVictoriasView;
        public Result mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPosicionView = (TextView)  view.findViewById(R.id.posicion_clasificacion);
            mNombreView = (TextView)  view.findViewById(R.id.piloto_constructor_clasificacion);
            mEscuderiaView = (TextView)  view.findViewById(R.id.constructor_nacionalidad_clasificacion);
            mPuntosView = (TextView)  view.findViewById(R.id.puntos_clasificacion);
            mVictoriasView = (TextView)  view.findViewById(R.id.victorias_clasificacion);

        }

    }

    public void swap(List<Result> items, List<Driver> drivers, List<Constructor> constructors){
        mValues = items;
        mDrivers = drivers;
        mConstructors = constructors;
        notifyDataSetChanged();

    }

}

