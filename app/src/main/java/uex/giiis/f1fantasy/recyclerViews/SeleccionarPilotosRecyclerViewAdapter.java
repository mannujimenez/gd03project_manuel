package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.Driver;


public class SeleccionarPilotosRecyclerViewAdapter extends RecyclerView.Adapter<SeleccionarPilotosRecyclerViewAdapter.ViewHolder> {

    private List<Driver> mValues;
    private Context mContext;
    private List<String> pilotosUsuario;
    public interface OnSeleccionarPilotosInteractionListener{
        public void OnSeleccionarPilotosInteractionListener(String driver_name, boolean _checked);
    }

    public OnSeleccionarPilotosInteractionListener mListener;

    public SeleccionarPilotosRecyclerViewAdapter(List<Driver> items, ArrayList<String> _pilotosUsuario,OnSeleccionarPilotosInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
        pilotosUsuario = _pilotosUsuario;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.piloto_seleccion_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Driver item = mValues.get(position);

        holder.mItem = item;
        holder.mNombreView.setText(item.getGivenName() + " " + item.getFamilyName());
        holder.mMultiplicadorView.setText(item.getNationality());


        //Si el usuario tiene al piloto marcado como fav
        if(pilotosUsuario.contains(item.getDriverId())){
            holder.mCheckView.setChecked(true);
        } else{
            holder.mCheckView.setChecked(false);
        }

        holder.mCheckView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                   if (null != mListener) {
                       if(pilotosUsuario.size() == 3){
                           holder.mCheckView.setChecked(false);
                       }

                       // Notify the active callbacks interface (the activity, if the
                       // fragment is attached to one) that an item has been selected.
                       mListener.OnSeleccionarPilotosInteractionListener(item.getDriverId(), holder.mCheckView.isChecked());
                   }
               }
           }
        );
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        holder.mCheckView.setOnCheckedChangeListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }


    public List<Driver> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombreView;
        public final TextView mMultiplicadorView;
        public final CheckBox mCheckView;
        public Driver mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombreView = (TextView)  view.findViewById(R.id.nombre_piloto);
            mMultiplicadorView = (TextView) view.findViewById(R.id.multiplicador_piloto);
            mCheckView = (CheckBox) view.findViewById(R.id.check_piloto);

        }

    }

    public void swap(List<Driver> items){
        mValues = items;
        notifyDataSetChanged();
    }

    public void pilotosSeleccionadorsUsuario(List<String> _pilotosUsuario){
        pilotosUsuario = _pilotosUsuario;
    }
}

