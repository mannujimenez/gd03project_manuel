package uex.giiis.f1fantasy.recyclerViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.ui.DetallesLigaActivity;

public class DetallesLigaRecyclerViewAdapter extends RecyclerView.Adapter<DetallesLigaRecyclerViewAdapter.ViewHolder> {

    private List<UserLeagues> mValues;
    private Context mContext;


    public DetallesLigaRecyclerViewAdapter(List<UserLeagues> items,  Context Context){
        mValues = items;
        mContext = Context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.usuario_clasificacion_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        UserLeagues item = mValues.get(position);

        holder.mItem = item;
        if(item != null) {
            holder.mPuestoLiga.setText(Integer.toString(position + 1));

            AppExecutors.getInstance().diskIO().execute(() -> {

                User us = new User();

                F1Database f1Database = F1Database.getInstance(mContext);

                //Conseguir los UserLeagues de los usuarios que pertenecen a la liga
                us = f1Database.userDao().getUserId(item.getUser());

                String username = us.getUsername();

                ((DetallesLigaActivity) mContext).runOnUiThread(() -> holder.mUsuarioLiga.setText(username));
            });

            holder.mPuntuacionLiga.setText(Integer.toString(item.getPoints()));
        }

    }

    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }

    public List<UserLeagues> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mPuestoLiga;
        public final TextView mUsuarioLiga;
        public final TextView mPuntuacionLiga;
        public UserLeagues mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPuestoLiga = (TextView)  view.findViewById(R.id.puesto_liga);
            mUsuarioLiga = (TextView)  view.findViewById(R.id.usuario_liga);
            mPuntuacionLiga = (TextView)  view.findViewById(R.id.puntuacion_liga);

        }

    }

    public void swap(List<UserLeagues> items){
        mValues = items;
        notifyDataSetChanged();
    }

}
