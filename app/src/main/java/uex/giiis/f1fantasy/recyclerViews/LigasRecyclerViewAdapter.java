package uex.giiis.f1fantasy.recyclerViews;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.pojo.League;


public class LigasRecyclerViewAdapter extends RecyclerView.Adapter<LigasRecyclerViewAdapter.ViewHolder> {

    private List<League> mValues;
    private Context mContext;

    public interface OnListInteractionListener{
        public void onListLigas(int id);
        public void onListCalendario(String id);
    }

    public OnListInteractionListener mListener;

    public LigasRecyclerViewAdapter(List<League> items, OnListInteractionListener listener, Context context) {
        mValues = items;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_liga, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        League item = mValues.get(position);

        holder.mItem = item;
        holder.mNombreView.setText(item.getName());

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListLigas(item.getId());
            }
        });

    }


    @Override
    public int getItemCount() {
        int resultado = 0;
        if(mValues != null){
            resultado = mValues.size();
        }
        return resultado;
    }


    public List<League> getmValues() {
        return mValues;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNombreView;
        public League mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNombreView = (TextView)  view.findViewById(R.id.nombre_liga);

        }

    }

    public void swap(List<League> items){
        mValues = items;
        notifyDataSetChanged();
    }

}

