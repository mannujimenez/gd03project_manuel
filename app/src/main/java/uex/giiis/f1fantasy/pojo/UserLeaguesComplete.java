package uex.giiis.f1fantasy.pojo;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;

public class UserLeaguesComplete {

    @Embedded
    public UserLeagues userLeagues;
    @Relation(
            parentColumn = "driver1",
            entityColumn = "driverId"
    )
    public Driver driver1;

    @Relation(
            parentColumn = "driver2",
            entityColumn = "driverId"
    )
    public Driver driver2;

    @Relation(
            parentColumn = "driver3",
            entityColumn = "driverId"
    )
    public Driver driver3;

    @Relation(
            parentColumn = "user",
            entityColumn = "id"
    )
    public User user;


    public UserLeagues getUserLeagues() {
        return userLeagues;
    }

    public void setUserLeagues(UserLeagues userLeagues) {
        this.userLeagues = userLeagues;
    }

    public Driver getDriver1() {
        return driver1;
    }

    public void setDriver1(Driver driver1) {
        this.driver1 = driver1;
    }

    public Driver getDriver2() {
        return driver2;
    }

    public void setDriver2(Driver driver2) {
        this.driver2 = driver2;
    }

    public Driver getDriver3() {
        return driver3;
    }

    public void setDriver3(Driver driver3) {
        this.driver3 = driver3;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}