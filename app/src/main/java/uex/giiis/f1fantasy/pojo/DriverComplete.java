package uex.giiis.f1fantasy.pojo;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;

public class DriverComplete {

    @Embedded
    public Driver driver;
    @Relation(
            parentColumn = "driverId",
            entityColumn = "idDriver"
    )
    public List<DriverConstructors> driverConstructors;
    @Relation(
            parentColumn = "idConstructor",
            entityColumn = "constructorId"
    )
    public Constructor constructor;

    public Constructor getConstructor() {
        return constructor;
    }

    public void setConstructor(Constructor constructor) {
        this.constructor = constructor;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public List<DriverConstructors> getDriverConstructors() {
        return driverConstructors;
    }

    public void setDriverConstructors(List<DriverConstructors> driverConstructors) {
        this.driverConstructors = driverConstructors;
    }
}