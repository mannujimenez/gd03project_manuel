package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MainActivity;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.EscuderiasViewModel;
import uex.giiis.f1fantasy.viewModels.LoginViewModel;

public class LoginActivity extends AppCompatActivity {


    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = LoginActivity.this.getSharedPreferences("usuario", LoginActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if(id_usuario == -1){
            setContentView(R.layout.login_layout);
        } else{ //Si hay un usuario logeado va al mainactivity
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
        }

    }


    public void iniciarSesion(View v){

        EditText usuario = (EditText) findViewById(R.id.usuario);
        EditText password = (EditText) findViewById(R.id.password);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        LoginViewModel mViewModel = new ViewModelProvider(this, appContainer.loginFactory)
                .get(LoginViewModel.class);
        mViewModel.setUsername(usuario.getText().toString());
        mViewModel.getUser().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if(user != null && user.getPassword().equals(password.getText().toString())){
                    //usar preferencias del usuario
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putInt("id_usuario", user.getId());
                    editor.commit();
                    iniciarMainActivity();
                }else{
                    TextView errorView = (TextView) findViewById(R.id.errorLogin);
                    errorView.setText(R.string.login_failed);
                }
            }
        });



    }

    public void irRegistro(View v){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    public void iniciarMainActivity(){
        Intent intent = new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


}