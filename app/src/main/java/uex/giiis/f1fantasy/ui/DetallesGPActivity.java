package uex.giiis.f1fantasy.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.Helper;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.recyclerViews.GPRecyclerViewAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.CalendarioViewModel;
import uex.giiis.f1fantasy.viewModels.DetallesGPViewModel;

import static uex.giiis.f1fantasy.ColeccionPaises.obtenerCircuito;
import static uex.giiis.f1fantasy.ColeccionPaises.obtenerPais;

public class DetallesGPActivity extends AppCompatActivity {

    private GPRecyclerViewAdapter adapter;
    private List<Result> results;
    private List<Driver> drivers;
    private List<Constructor> constructors;
    boolean proximo;
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.granpremio_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        Bundle bundle = getIntent().getExtras();
        String round = bundle.getString("ID_GP");

        TextView nombre = (TextView) findViewById(R.id.nombre_granpremio);
        TextView ronda = (TextView) findViewById(R.id.texto_ronda);
        TextView circuito = (TextView) findViewById(R.id.texto_circuito);
        TextView fecha = (TextView) findViewById(R.id.texto_fecha);
        TextView hora = (TextView) findViewById(R.id.texto_hora);
        TextView localidad = (TextView) findViewById(R.id.texto_localidad);
        TextView pais = (TextView) findViewById(R.id.texto_pais);
        ImageView imagen = (ImageView) findViewById(R.id.imagen_granpremio);
        ImageView bandera = (ImageView) findViewById(R.id.bandera_granpremio);
        TextView tituloResultado = (TextView) findViewById(R.id.titulo_resultado_gp);
        proximo = false;

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_resultado);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        adapter = new GPRecyclerViewAdapter(results, drivers, constructors, this);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        DetallesGPViewModel mViewModel = new ViewModelProvider(this, appContainer.detallesGPFactory)
                .get(DetallesGPViewModel.class);
        mViewModel.setGpID(Integer.parseInt(round));
        mViewModel.getRace().observe(this, new Observer<Race>() {
            @Override
            public void onChanged(Race race) {
                nombre.setText(race.getRaceName());
                ronda.setText(race.getRound());
                circuito.setText(race.getCircuit().getCircuitName());
                fecha.setText(race.getDate());
                hora.setText(race.getTime());
                localidad.setText(race.getCircuit().getLocation().getLocality());
                pais.setText(race.getCircuit().getLocation().getCountry());
                imagen.setImageResource(obtenerCircuito(race.getCircuit().getCircuitId()));
                bandera.setImageResource(obtenerPais(race.getCircuit().getLocation().getCountry()));
            }
        });

        mViewModel.getDrivers().observe(this, new Observer<List<Driver>>(){
            @Override
            public void onChanged(List<Driver> driverList) {
                drivers = driverList;
                adapter.swap(results, drivers, constructors);
            }
        });

        mViewModel.getConstructors().observe(this, new Observer<List<Constructor>>(){
            @Override
            public void onChanged(List<Constructor> constructorList) {
                constructors = constructorList;
                adapter.swap(results, drivers, constructors);
            }
        });

        mViewModel.getResults().observe(this, new Observer<List<Result>>(){
            @Override
            public void onChanged(List<Result> resultList) {
                results = resultList;
                Collections.sort(results,new Comparator<Result>(){
                    @Override
                    public int compare(final Result lhs,Result rhs) {
                        if(Integer.parseInt(lhs.getPosition()) < Integer.parseInt(rhs.getPosition())) {
                            return -1;
                        } else
                            return 1;
                    }
                });
                adapter.swap(results, drivers, constructors);
            }
        });

        SharedPreferences sharedPreferences = this.getSharedPreferences("ultimaModificacion", this.MODE_PRIVATE);
        int rondaPreferencias = sharedPreferences.getInt("ULTIMA_RONDA_ACTUALIZADA",-1);
        if (Integer.parseInt(round) > rondaPreferencias) {
            tituloResultado.setText(R.string.sin_resultado_gp);
        }

        recyclerView.setAdapter(adapter);
        adapter.swap(results, drivers, constructors);
    }



    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void cerrarSesion (MenuItem item) {
        sharedPref = DetallesGPActivity.this.getSharedPreferences("usuario", DetallesGPActivity.this.MODE_PRIVATE);

        int id_usuario = sharedPref.getInt("id_usuario", -1);

        if (id_usuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

}