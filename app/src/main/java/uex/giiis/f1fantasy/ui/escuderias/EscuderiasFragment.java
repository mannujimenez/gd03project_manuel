package uex.giiis.f1fantasy.ui.escuderias;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uex.giiis.f1fantasy.AppContainer;
import uex.giiis.f1fantasy.MyApplication;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;
import uex.giiis.f1fantasy.recyclerViews.EscuderiasExpandableListAdapter;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.viewModels.ClasificacionesPilotosViewModel;
import uex.giiis.f1fantasy.viewModels.EscuderiasViewModel;

public class EscuderiasFragment extends Fragment {

    private EscuderiasViewModel escuderiasViewModel;

    private EscuderiasExpandableListAdapter adapter;
    private List<Constructor> constructorList;
    private Map<Constructor, List<Driver>> mapConstructorDrivers;
    private EscuderiasExpandableListAdapter.OnListPilotosListener mCallback;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        constructorList = new ArrayList<Constructor>();
        mapConstructorDrivers = new HashMap<>();
        constructorsDatabase();

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_escuderias, container, false);

        ExpandableListView expandableListView = (ExpandableListView) view.findViewById(R.id.list_escuderias);


        adapter = new EscuderiasExpandableListAdapter(constructorList, mapConstructorDrivers, mCallback, getContext());
        expandableListView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EscuderiasExpandableListAdapter.OnListPilotosListener) {
            mCallback = (EscuderiasExpandableListAdapter.OnListPilotosListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListPilotosListener");
        }
    }

    private void actualizarExpandableView(List<Constructor> constructorList, Map<Constructor, List<Driver>> mapConstructorDrivers) {
        adapter.swap(constructorList, mapConstructorDrivers);
    }

    public void constructorsDatabase () {

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        EscuderiasViewModel mViewModel = new ViewModelProvider(this, appContainer.escuderiasFactory)
                .get(EscuderiasViewModel.class);
        mViewModel.getConstructorCompleteList().observe(this, new Observer<List<ConstructorComplete>>() {

            @Override
            public void onChanged(List<ConstructorComplete> constructorCompleteList) {
                constructorList.clear();
                mapConstructorDrivers.clear();
                for (ConstructorComplete c: constructorCompleteList) {
                    constructorList.add(c.constructor);
                    mapConstructorDrivers.put(c.getConstructor(), c.getDriverList());
                }

                actualizarExpandableView(constructorList, mapConstructorDrivers);
            }
        });

     }
}