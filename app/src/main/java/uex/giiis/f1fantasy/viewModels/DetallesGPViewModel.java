package uex.giiis.f1fantasy.viewModels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.ui.DetallesGPActivity;
import uex.giiis.f1fantasy.repository.F1Repository;

/**
 * {@link ViewModel} for {@link DetallesGPActivity}
 */
public class DetallesGPViewModel extends ViewModel {

    private final F1Repository mRepository;
    private LiveData<Race> mRace;
    private final LiveData<List<Driver>> mDrivers;
    private final LiveData<List<Constructor>> mConstructors;
    private LiveData<List<Result>> mResults;
    private int mGpID;


    public DetallesGPViewModel(F1Repository repository) {
        mRepository = repository;
        mRace = mRepository.getRace();
        mDrivers = mRepository.getCurrentDrivers();
        mConstructors = mRepository.getCurrentConstructors();
        mResults = mRepository.getResults();
    }

    public void setGpID(int gpID){
        mGpID = gpID;
        mRepository.setGpId(gpID);
    }

    public LiveData<Race> getRace() {
        return mRace;
    }

    public LiveData<List<Driver>> getDrivers() {
        return mDrivers;
    }

    public LiveData<List<Constructor>> getConstructors() {
        return mConstructors;
    }

    public LiveData<List<Result>> getResults() {
        return mResults;
    }

}