package uex.giiis.f1fantasy.viewModels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.pojo.ConstructorComplete;
import uex.giiis.f1fantasy.ui.escuderias.EscuderiasFragment;
import uex.giiis.f1fantasy.repository.F1Repository;

/**
 * {@link ViewModel} for {@link EscuderiasFragment}
 */
public class EscuderiasViewModel extends ViewModel {

    private final F1Repository mRepository;
    private final LiveData<List<ConstructorComplete>> mConstructorCompleteList;


    public EscuderiasViewModel(F1Repository repository) {
        mRepository = repository;
        mConstructorCompleteList = mRepository.getConstructorCompleteList();
    }

    public LiveData<List<ConstructorComplete>> getConstructorCompleteList() {
        return mConstructorCompleteList;
    }

}