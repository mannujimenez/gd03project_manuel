package uex.giiis.f1fantasy.viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.pojo.ConstructorStandingComplete;
import uex.giiis.f1fantasy.repository.F1Repository;
import uex.giiis.f1fantasy.ui.clasificaciones.ClasificacionesConstructores;

/**
 * {@link ViewModel} for {@link ClasificacionesConstructores}
 */
public class ClasificacionesConstructoresViewModel extends ViewModel {

    private final F1Repository mRepository;
    private final LiveData<List<ConstructorStandingComplete>> mConstructorStandingsCompleteList;

    public ClasificacionesConstructoresViewModel(F1Repository repository) {
        mRepository = repository;
        mConstructorStandingsCompleteList = mRepository.getCurrentConstructorStanding();
    }

    public LiveData<List<ConstructorStandingComplete>> getConstructorStandingsCompleteList() {
        return mConstructorStandingsCompleteList;
    }

}