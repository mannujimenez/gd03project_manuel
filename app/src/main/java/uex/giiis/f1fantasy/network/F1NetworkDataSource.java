package uex.giiis.f1fantasy.network;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import uex.giiis.f1fantasy.executors.AppExecutors;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStanding;
import uex.giiis.f1fantasy.generatedPojos.ConstructorStandingsList;
import uex.giiis.f1fantasy.generatedPojos.ConstructorTable;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.generatedPojos.Race;
import uex.giiis.f1fantasy.generatedPojos.Result;
import uex.giiis.f1fantasy.generatedPojos.StandingsList;
import uex.giiis.f1fantasy.pojo.DriverConstructors;
import uex.giiis.f1fantasy.pojo.Information;
import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.roomdb.F1Database;

public class F1NetworkDataSource {
    private static final String LOG_TAG = F1NetworkDataSource.class.getSimpleName();
    private static F1NetworkDataSource sInstance;

    private F1Database f1Database;
    private Context context;

    private final MutableLiveData<StandingsList> mStandingList;
    private final MutableLiveData<ConstructorStandingsList> mConstructorStandingsList;
    private final MutableLiveData<List<ConstructorTable>> mListDriversConstructors;
    private final MutableLiveData<List<Race>> mResults;
    private final MutableLiveData<List<Race>> mRaces;
    private final MutableLiveData<Information> mInformation;


    private F1NetworkDataSource(Context _context) {

        f1Database = F1Database.getInstance(context);

        mStandingList = new MutableLiveData<>();
        mConstructorStandingsList = new MutableLiveData<>();
        mListDriversConstructors = new MutableLiveData<>();
        mResults = new MutableLiveData<>();
        mRaces = new MutableLiveData<>();
        mInformation = new MutableLiveData<>();

        context = _context;
    }

    public synchronized static F1NetworkDataSource getInstance(Context _context) {
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new F1NetworkDataSource(_context);
            Log.d(LOG_TAG, "Made new network data source");
        }

        return sInstance;
    }

    public LiveData<StandingsList> getmStandingList() {
        return mStandingList;
    }

    public MutableLiveData<ConstructorStandingsList> getmConstructorStandingsList() {
        return mConstructorStandingsList;
    }

    public MutableLiveData<List<ConstructorTable>> getmListDriversConstructors() {
        return mListDriversConstructors;
    }

    public MutableLiveData<List<Race>> getmResults() {
        return mResults;
    }

    public MutableLiveData<List<Race>> getmRaces() {
        return mRaces;
    }

    public MutableLiveData<Information> getmInformation() {
        return mInformation;
    }

    public void fetchAPIData(Context _context) {

        AppExecutors.getInstance().networkIO().execute(new LoaderRunnable(f1Database, _context, new onStandingListListener() {
            @Override
            public void onStandingListLoaded(StandingsList st) {
                mStandingList.setValue(st);
            }

            @Override
            public void onConstructorStandingsList(ConstructorStandingsList constructorStandingsList) {
                mConstructorStandingsList.setValue(constructorStandingsList);
            }

            @Override
            public void onListDriversConstructors(List<ConstructorTable> listDriversConstructors) {
                mListDriversConstructors.setValue(listDriversConstructors);
            }

            @Override
            public void onListRaceResults(List<Race> results) {
                mResults.setValue(results);
            }

            @Override
            public void onListRace(List<Race> races) {
                mRaces.setValue(races);
            }

            @Override
            public void onInformation(Information information) {
                mInformation.setValue(information);
            }
        }));
    }

    public void saveDriversAndConstructors(StandingsList standingsList){

        Driver driver;
        Constructor constructor;
        List<DriverStanding> driverStandings = standingsList.getDriverStandings();
        List<Driver> drivers = new ArrayList<Driver>();
        List<Constructor> constructors = new ArrayList<Constructor>();
        for ( DriverStanding d : driverStandings ) {
            driver = d.getDriver();
            constructor = d.getConstructors().get(0);
            driver.setIdConstructor(constructor.getConstructorId());
            drivers.add(driver);
            constructors.add(constructor);
        }
        f1Database.constructorDao().insertAll(constructors);
        f1Database.driverDao().insertAll(drivers);

    }

    public void saveDriverConstructors(List<ConstructorTable> listDriversConstructors){

        List<DriverConstructors> driverConstructors = new ArrayList<DriverConstructors>();
        List<Constructor> constructors;
        String idDriver;
        String idConstructor;
        for ( ConstructorTable constructorTable : listDriversConstructors ) {
            constructors = constructorTable.getConstructors();
            idDriver = constructorTable.getDriverId();
            for ( Constructor constructor : constructors ) {
                DriverConstructors driverConstructor = new DriverConstructors();
                driverConstructor.setIdConstructor(constructor.getConstructorId());
                driverConstructor.setIdDriver(idDriver);
                driverConstructors.add(driverConstructor);
            }
        }

        f1Database.driverConstructorsDao().insertAll(driverConstructors);

    }

    public void saveResults(List<Race> results){

        List<Result> allResults = new ArrayList<Result>();
        List<Result> raceResults = new ArrayList<Result>();
        String round;
        for ( Race race : results ) {
            raceResults = race.getResults();
            round = race.getRound();
            for ( Result r : raceResults ) {
                r.setIdConstructor(r.getConstructor().getConstructorId());
                r.setIdDriver(r.getDriver().getDriverId());
                r.setRound(round);
                allResults.add(r);
            }
        }
        f1Database.resultDao().insertAll(allResults);

    }

    public void saveRaces(List<Race> races){
        f1Database.raceDao().insertAll(races);
    }

    public void saveConstructorStandings (ConstructorStandingsList constructorStandingsList) {

        List<ConstructorStanding> allConstructorStandings = new ArrayList<ConstructorStanding>();

        List<ConstructorStanding> constructorStandings = constructorStandingsList.getConstructorStandings();

        //Clasificaciones de constructores de ronda actual
        for ( ConstructorStanding c : constructorStandings ) {
            c.setIdConstructor(c.getConstructor().getConstructorId());
            c.setRound(constructorStandingsList.getRound());
            allConstructorStandings.add(c);
        }

        f1Database.constructorStandingDao().insertAll(allConstructorStandings);

    }

    public void saveDriverStandings (StandingsList standingsList) {

        List<DriverStanding> allDriverStandings = new ArrayList<DriverStanding>();

        List<DriverStanding> driverStandings = standingsList.getDriverStandings();

        //Clasificaciones de pilotos de ronda actual
        for ( DriverStanding d : driverStandings ) {
            d.setIdDriver(d.getDriver().getDriverId());
            d.setIdConstructor(d.getDriver().getIdConstructor());
            d.setRound(standingsList.getRound());
            allDriverStandings.add(d);
        }


        f1Database.driverStandingDao().insertAll(allDriverStandings);
    }



    public void saveInformation (StandingsList standingsList){

        Information info = new Information();
        info.setRound(standingsList.getRound());
        info.setSeason(standingsList.getSeason());

        f1Database.informationDao().insert(info);
    }


    public void actualizarPuntosUserLeague(StandingsList standingsList){

        int rondaActual = Integer.parseInt(standingsList.getRound());

        String positionStanding;
        String positionRace = " ";
        List<Result> resultList = new ArrayList<Result>();
        List<UserLeagues> uLeagues = f1Database.userLeaguesDao().getAll();

        for (UserLeagues ul: uLeagues) {

            int points = ul.getPoints();

            //Piloto 1
            if(ul.getDriver1() != null) {
                //Posicion piloto 1 en el campeonato
                positionStanding = f1Database.driverStandingDao().get(ul.getDriver1(), rondaActual).getPosition();
                resultList = f1Database.resultDao().getFromGP2(rondaActual);
                for (Result r : resultList) {

                    if (ul.getDriver1().equals(r.getIdDriver())) {
                        positionRace = r.getPosition();
                    }
                }
                if(!positionRace.equals(" ")){
                        points = points + (100/Integer.parseInt(positionRace)) - (60/Integer.parseInt(positionStanding));
                }
                ul.setDriver1(null);
            }
            positionRace = " ";

            //Piloto 2
            if(ul.getDriver2() != null) {
                //Posicion piloto 2 en el campeonato
                positionStanding = f1Database.driverStandingDao().get(ul.getDriver2(), rondaActual).getPosition();
                resultList = f1Database.resultDao().getFromGP2(rondaActual);
                for (Result r: resultList) {

                    if(ul.getDriver2().equals(r.getIdDriver())){
                        positionRace = r.getPosition();
                    }
                }
                if(!positionRace.equals(" ")){
                        points = points + (100/Integer.parseInt(positionRace)) - (60/Integer.parseInt(positionStanding));
                }
                ul.setDriver2(null);
            }
            positionRace = " ";

            //Piloto 3
            if(ul.getDriver3() != null) {
                //Posicion piloto 2 en el campeonato
                positionStanding = f1Database.driverStandingDao().get(ul.getDriver3(), rondaActual).getPosition();
                resultList = f1Database.resultDao().getFromGP2(rondaActual);
                for (Result r: resultList) {

                    if(ul.getDriver3().equals(r.getIdDriver())){
                        positionRace = r.getPosition();
                    }
                }
                if(!positionRace.equals(" ")){
                        points = points + (100/Integer.parseInt(positionRace)) - (60/Integer.parseInt(positionStanding));
                }
                ul.setDriver3(null);
            }

            //Actualizar puntos del UserLeague
            ul.setPoints(points);
            f1Database.userLeaguesDao().insert(ul);
        }

    }



}
