package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import kotlin.Triple;
import uex.giiis.f1fantasy.generatedPojos.Constructor;
import uex.giiis.f1fantasy.generatedPojos.Driver;
import uex.giiis.f1fantasy.generatedPojos.DriverStanding;
import uex.giiis.f1fantasy.pojo.DriverStandingComplete;

@Dao
public interface DriverStandingDao {

    @Query("SELECT * FROM DriverStanding")
    public List<DriverStanding> getAll();

    @Query("SELECT * FROM DriverStanding WHERE idDriver = :idDriver")
    public List<DriverStanding> getFromDriver(String idDriver);

    @Query("SELECT * FROM DriverStanding WHERE round = :idStanding")
    public LiveData<List<DriverStanding>> getFromStanding(int idStanding);

    @Transaction
    @Query("SELECT * FROM DriverStanding WHERE round = :idStanding")
    public LiveData<List<DriverStandingComplete>> getFromStandingComplete(int idStanding);

    /*@Query("SELECT DriverStanding.position, DriverStanding.points, Driver.givenName || ' ' || Driver.familyName, Constructor.name " +
            "FROM DriverStanding " +
            "JOIN Driver ON DriverStanding.idDriver = Driver.driverId " +
            "JOIN Constructor ON Driver.idconstructor = Constructor.constructorId " +
            "WHERE DriverStanding.round = :idStanding;")
    public LiveData<List<DriverStandingComplete>> getFromStandingComplete(int idStanding);*/

    @Query("SELECT * FROM DriverStanding WHERE idDriver = :idDriver AND round = :idStanding")
    public DriverStanding get(String idDriver, int idStanding);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert (DriverStanding driverPosition);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertAll (List<DriverStanding> driverPosition);

    @Update
    public int update (DriverStanding driverPosition);

    @Delete
    public int delete (DriverStanding driverPosition);
}
