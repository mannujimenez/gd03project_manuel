package uex.giiis.f1fantasy.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

import uex.giiis.f1fantasy.pojo.UserLeagues;
import uex.giiis.f1fantasy.pojo.UserLeaguesComplete;

@Dao
public interface UserLeaguesDao {

    @Query("SELECT * FROM userleagues")
    public List<UserLeagues> getAll();

    @Query("SELECT * FROM userleagues WHERE user = :idUser")
    public LiveData<List<UserLeagues>> getFromUser(int idUser);

    @Transaction
    @Query("SELECT * FROM userleagues WHERE league = :idLeague")
    public LiveData<List<UserLeaguesComplete>> getFromLeague(int idLeague);

    @Query("SELECT * FROM userleagues WHERE league = :idLeague ORDER BY points desc")
    public LiveData<List<UserLeagues>> getFromLeaguePoints(int idLeague);

    @Query("SELECT * FROM userleagues WHERE user = :idUser AND league = :idLeague")
    public LiveData<UserLeagues> get(int idUser, int idLeague);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert (UserLeagues userLeagues);

    @Update
    public int update (UserLeagues userLeagues);

    @Delete
    public int delete (UserLeagues userLeagues);

    @Query("DELETE FROM userleagues WHERE league = :idLeague AND user = :idUser")
    public void delete (int idLeague, int idUser);

    @Query("DELETE FROM userleagues WHERE user = :idUser")
    public int delete (int idUser);
}
