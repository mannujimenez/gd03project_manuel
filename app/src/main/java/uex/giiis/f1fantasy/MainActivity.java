package uex.giiis.f1fantasy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import uex.giiis.f1fantasy.recyclerViews.EscuderiasExpandableListAdapter;
import uex.giiis.f1fantasy.recyclerViews.LigasRecyclerViewAdapter;
import uex.giiis.f1fantasy.ui.DetallesGPActivity;
import uex.giiis.f1fantasy.ui.DetallesLigaActivity;
import uex.giiis.f1fantasy.ui.DetallesPilotoActivity;
import uex.giiis.f1fantasy.ui.LoginActivity;
import uex.giiis.f1fantasy.ui.PerfilActivity;
import uex.giiis.f1fantasy.ui.popUps.CrearLigaPopup;
import uex.giiis.f1fantasy.ui.popUps.UnirseLigaPopup;


public class MainActivity extends AppCompatActivity implements LigasRecyclerViewAdapter.OnListInteractionListener, EscuderiasExpandableListAdapter.OnListPilotosListener {

    LigasRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(navView, navController);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void crearLiga (View view) {
        Intent intent = new Intent(this, CrearLigaPopup.class);
        startActivity(intent);
    }

    public void editarPerfil (MenuItem item) {
        Intent intent = new Intent(this, PerfilActivity.class);
        startActivity(intent);
    }

    public void cerrarSesion (MenuItem item) {
        SharedPreferences sharedPref = MainActivity.this.getSharedPreferences("usuario", MainActivity.this.MODE_PRIVATE);

        int idUsuario = sharedPref.getInt("id_usuario", -1);

        if (idUsuario != -1) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt("id_usuario", -1);
            editor.commit();

            Intent intent = new Intent (this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onListLigas(int id) {
        Intent intent = new Intent (this, DetallesLigaActivity.class);
        intent.putExtra("ID_LIGA", Integer.toString(id));
        startActivity(intent);
    }

    @Override
    public void onListCalendario(String id) {
        Intent intent = new Intent (this, DetallesGPActivity.class);
        intent.putExtra("ID_GP", id);
        startActivity(intent);
    }

    public void unirseLiga (View view) {
        Intent intent = new Intent(this, UnirseLigaPopup.class);
        startActivity(intent);
    }


    @Override
    public void onListPilotos(String id) {
        Intent intent = new Intent (this, DetallesPilotoActivity.class);
        intent.putExtra("ID_PILOTO", id);
        startActivity(intent);
    }
}