
package uex.giiis.f1fantasy.generatedPojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Formula1Results {

    @SerializedName("MRData")
    @Expose
    private MRDataResults mRDataResults;

    @JsonProperty("MRData")
    public MRDataResults getMRData() {
        return mRDataResults;
    }

    public void setMRData(MRDataResults mRDataResults) {
        this.mRDataResults = mRDataResults;
    }

}
