
package uex.giiis.f1fantasy.generatedPojos;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static androidx.room.ForeignKey.CASCADE;

@Entity(primaryKeys = {"round", "idDriver"},
        foreignKeys = {@ForeignKey(entity = Driver.class,
                parentColumns = "driverId",
                childColumns = "idDriver",
                onDelete = CASCADE)})
public class DriverStanding {

    @SerializedName("position")
    @Expose
    private String position;
    @Ignore
    @SerializedName("positionText")
    @Expose
    private String positionText;
    @SerializedName("points")
    @Expose
    private String points;
    @SerializedName("wins")
    @Expose
    private String wins;
    @Ignore
    @SerializedName("Driver")
    @Expose
    private Driver driver;
    @Ignore
    @SerializedName("Constructors")
    @Expose
    private List<Constructor> constructors = null;

    @NonNull
    private String idDriver;

    private String idConstructor;

    @NonNull
    private String round;

    public String getIdDriver() {
        return idDriver;
    }

    public void setIdDriver(String idDriver) {
        this.idDriver = idDriver;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    @JsonProperty("position")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @JsonProperty("positionText")
    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    @JsonProperty("points")
    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    @JsonProperty("wins")
    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }

    @JsonProperty("Driver")
    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    @JsonProperty("Constructors")
    public List<Constructor> getConstructors() {
        return constructors;
    }

    public void setConstructors(List<Constructor> constructors) {
        this.constructors = constructors;
    }

    public String getIdConstructor() {
        return idConstructor;
    }

    public void setIdConstructor(@NonNull String idConstructor) {
        this.idConstructor = idConstructor;
    }
}
