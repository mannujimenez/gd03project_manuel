
package uex.giiis.f1fantasy.generatedPojos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Formula1Calendar {

    @SerializedName("MRData")
    @Expose
    private MRDataCalendar mRDataCalendar;

    @JsonProperty("MRData")
    public MRDataCalendar getMRData() {
        return mRDataCalendar;
    }

    public void setMRData(MRDataCalendar mRDataCalendar) {
        this.mRDataCalendar = mRDataCalendar;
    }

}
