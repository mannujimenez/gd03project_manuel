package uex.giiis.f1fantasy.UC04tests;

import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.ui.LoginActivity;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ElegirPilotosTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void shouldChooseDrivers() {

        onView(withId(R.id.boton_registro_login)).perform(click());

        onView(withId(R.id.usuario_registrar)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password_registrar)).perform(typeText("1234"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        boolean salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        onView(withId(R.id.navigation_ligas)).perform(click());

        // Creamos una liga
        onView(withId(R.id.boton_crear_liga)).perform(click());
        onView(withId(R.id.editar_nombre_crear)).perform(typeText("Liga de Prueba"));
        onView(withId(R.id.editar_password_crear)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_crear_liga_liga)).perform(click());

        // Comprobamos que la liga se encuentre en nuestra lista de ligas
        onView(withId(R.id.list_ligas)).check(matches(hasDescendant(withId(R.id.nombre_liga))));
        onView(withId(R.id.list_ligas)).check(matches(hasDescendant(withText("Liga de Prueba"))));

        onView(withId(R.id.list_ligas)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        onView(withId(R.id.boton_elegir_pilotos)).perform(click());

        //Se marca el piloto con la posición 0
        ViewInteraction appCompatCheckBox = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        0),
                                2)));
        appCompatCheckBox.perform(scrollTo(), click());

        //Se marca el piloto con la posición 1
        ViewInteraction appCompatCheckBox2 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        1),
                                2)));
        appCompatCheckBox2.perform(scrollTo(), click());

        //Se marca el piloto con la posición 2
        ViewInteraction appCompatCheckBox3 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        2),
                                2)));
        appCompatCheckBox3.perform(scrollTo(), click());

        //Se intenta marcar el piloto con la posición 3, pero como ya se han elegido 3, el máximo, no es posible marcar este último
        ViewInteraction appCompatCheckBox4 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        3),
                                2)));
        appCompatCheckBox4.perform(scrollTo(), click());

        //Se confirman los pilotos elegidos
        ViewInteraction appCompatButton6 = onView(
                allOf(withId(R.id.boton_aceptar_pilotos), withText("Aceptar"),
                        childAtPosition(
                                allOf(withId(R.id.botones_elegir_pilotos),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                1)),
                                0),
                        isDisplayed()));
        appCompatButton6.perform(click());

        //Se vuelve a entrar a la funcionalidad de selección para comprobar que los pilotos elegidos se han guardado
        onView(withId(R.id.boton_elegir_pilotos)).perform(click());

        //Comprobación de que el piloto de la posición 0 efectivamente está marcado
        ViewInteraction comprobacion1 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        0),
                                2)));
        comprobacion1.check(matches(isChecked()));

        //Comprobación de que el piloto de la posición 1 efectivamente está marcado
        ViewInteraction comprobacion2 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        1),
                                2)));
        comprobacion2.check(matches(isChecked()));

        //Comprobación de que el piloto de la posición 2 efectivamente está marcado
        ViewInteraction comprobacion3 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        2),
                                2)));
        comprobacion3.check(matches(isChecked()));

        //Comprobación de que el piloto de la posición 3 no está marcado
        ViewInteraction comprobacion4 = onView(
                allOf(withId(R.id.check_piloto), withText(" "),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.list_pilotos),
                                        3),
                                2)));
        comprobacion4.check(matches(not(isChecked())));

        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withId(R.id.boton_borrar)).perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

}