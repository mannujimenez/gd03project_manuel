package uex.giiis.f1fantasy.UC02tests;

import androidx.test.InstrumentationRegistry;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.giiis.f1fantasy.MainActivity;
import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.pojo.League;
import uex.giiis.f1fantasy.pojo.User;
import uex.giiis.f1fantasy.roomdb.F1Database;
import uex.giiis.f1fantasy.ui.LoginActivity;
import uex.giiis.f1fantasy.ui.ligas.LigasFragment;
import uex.giiis.f1fantasy.ui.popUps.CrearLigaPopup;
import uex.giiis.f1fantasy.ui.popUps.UnirseLigaPopup;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class UnirseLigaTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule =
            new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void shouldAddLeagueToRecyclerView() {

        // Se registra el usuario 1
        onView(withId(R.id.boton_registro_login)).perform(click());
        onView(withId(R.id.usuario_registrar)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password_registrar)).perform(typeText("1234"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        boolean salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        // Creamos una liga
        onView(withId(R.id.boton_crear_liga)).perform(click());
        onView(withId(R.id.editar_nombre_crear)).perform(typeText("Liga de Prueba"));
        onView(withId(R.id.editar_password_crear)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_crear_liga_liga)).perform(click());

        // Abrimos el menú y cerramos sesión
        openContextualActionModeOverflowMenu();
        onView(withText(R.string.cerrar_sesion)).perform(click());

        // Nos registramos con otro usuario
        onView(withId(R.id.boton_registro_login)).perform(click());
        onView(withId(R.id.usuario_registrar)).perform(typeText("UserPrueba2"));
        onView(withId(R.id.password_registrar)).perform(typeText("1234"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        // Esperamos a que finalice la Splash Activity
        salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        // Nos unimos a la liga previamente creada por el otro usuario
        onView(withId(R.id.boton_unirse_liga)).perform(click());
        onView(withId(R.id.editar_nombre_unirse)).perform(typeText("Liga de Prueba"));
        onView(withId(R.id.editar_password_unirse)).perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.boton_unirse_liga_liga)).perform(click());

        // Comprobamos que la liga se encuentre en nuestra lista de ligas
        onView(withId(R.id.list_ligas)).check(matches(hasDescendant(withId(R.id.nombre_liga))));
        onView(withId(R.id.list_ligas)).check(matches(hasDescendant(withText("Liga de Prueba"))));

        // Abrimos el menú y borramos el usuario
        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withText(R.string.borrar_perfil)).perform(click());

        // Iniciamos sesión con el usuario 1
        onView(withId(R.id.usuario)).perform(typeText("UserPrueba"));
        onView(withId(R.id.password)).perform(typeText("1234"));
        onView(withId(R.id.boton_inicio_login)).perform(click());

        // Esperamos a la Splash Activity
        salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        // Lo borramos también
        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withText(R.string.borrar_perfil)).perform(click());

    }

}
