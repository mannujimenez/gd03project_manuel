package uex.giiis.f1fantasy.UC14tests;

import androidx.test.espresso.AmbiguousViewMatcherException;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uex.giiis.f1fantasy.R;
import uex.giiis.f1fantasy.ui.LoginActivity;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class PilotosYEscuderiasTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void shouldShowConstructorsAndDriversTest() {

        onView(withId(R.id.boton_registro_login)).perform(click());

        onView(withId(R.id.usuario_registrar)).perform(typeText("prueba"));
        onView(withId(R.id.password_registrar)).perform(typeText("gps"));
        onView(withId(R.id.password_confirmar_registrar)).perform(typeText("gps"), closeSoftKeyboard());
        onView(withId(R.id.boton_registro_online)).perform(click());

        boolean salir = false;
        while (!salir) {
            try {
                onView(withId(R.id.navigation_calendario)).check(matches(isDisplayed()));
                salir = true;
            } catch (NoMatchingViewException e) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }

        onView(withId(R.id.navigation_escuderias)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.list_escuderias)).atPosition(0).perform(click());
        boolean bandera = false;
        try{
            onView(withId(R.id.escuderias_group)).check(matches(not(withText("escuderia"))));
            bandera = true;
        }catch (AmbiguousViewMatcherException e) {
            //Si lanza la excepcion es que hay varias escuderias
        }

        try{
            onView(withId(R.id.escuderias_child)).check(matches(not(withText("escuderia"))));
            bandera = true;
        }catch (AmbiguousViewMatcherException e){
            //Si lanza la excepcion es que hay varios pilotos
            ;
        }

        if(bandera){
            throw new ExceptionInInitializerError("No hay pilotos");
        }

        openContextualActionModeOverflowMenu();
        onView(withText(R.string.perfil)).perform(click());
        onView(withId(R.id.boton_borrar)).perform(click());
    }

}