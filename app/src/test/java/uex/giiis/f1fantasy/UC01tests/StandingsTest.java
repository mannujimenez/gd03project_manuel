package uex.giiis.f1fantasy.UC01tests;

import uex.giiis.f1fantasy.generatedPojos.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StandingsTest {

    private DriverStanding driverStanding;
    private ConstructorStanding constructorStanding;

    @Test
    public void driverStandingShouldBeCreated() {
        driverStanding = new DriverStanding();
        driverStanding.setPosition("3");
        driverStanding.setIdDriver("sainz");
        driverStanding.setPoints("96");
        driverStanding.setRound("16");
        driverStanding.setWins("0");

        assertEquals("3", driverStanding.getPosition());
        assertEquals("sainz", driverStanding.getIdDriver());
        assertEquals("96", driverStanding.getPoints());
        assertEquals("16", driverStanding.getRound());
        assertEquals("0", driverStanding.getWins());

    }

    @Test
    public void constructorStandingShouldBeCreated() {
        constructorStanding = new ConstructorStanding();
        constructorStanding.setPosition("1");
        constructorStanding.setPoints("54");
        constructorStanding.setWins("1");
        constructorStanding.setIdConstructor("mercedes");
        constructorStanding.setRound("5");

        assertEquals("1", constructorStanding.getPosition());
        assertEquals("54", constructorStanding.getPoints());
        assertEquals("1", constructorStanding.getWins());
        assertEquals("mercedes", constructorStanding.getIdConstructor());
        assertEquals("5", constructorStanding.getRound());

    }
}
