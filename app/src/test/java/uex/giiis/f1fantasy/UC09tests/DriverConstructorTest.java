package uex.giiis.f1fantasy.UC09tests;

import uex.giiis.f1fantasy.pojo.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DriverConstructorTest {

    private DriverConstructors driverConstructors;

    @Test
    public void driverConstructorsShouldBeCreated() {
        driverConstructors = new DriverConstructors();
        driverConstructors.setIdDriver("hamilton");
        driverConstructors.setIdConstructor("mclaren");

        assertEquals("hamilton", driverConstructors.getIdDriver());
        assertEquals("mclaren", driverConstructors.getIdConstructor());
    }
}